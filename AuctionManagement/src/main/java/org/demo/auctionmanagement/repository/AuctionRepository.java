package org.demo.auctionmanagement.repository;

import org.demo.auctionmanagement.entity.AuctionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuctionRepository extends JpaRepository<AuctionEntity, String> {

}
