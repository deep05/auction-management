package org.demo.auctionmanagement.entity;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "auction")
public class AuctionEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String auctionItemId;

	@Column(name = "reservePrice", nullable = false)
	private Double reservePrice;

	@Column(name = "itemId", nullable = false)
	private String itemId;

	@Column(name = "itemDescription", nullable = false)
	private String itemDescription;

	@Column(name = "currentBid", nullable = false)
	private Double currentBid = 0d;

	@Column(name = "bidderName", nullable = true)
	private String bidderName;

	public Double getReservePrice() {
		return reservePrice;
	}

	public void setReservePrice(Double reservePrice) {
		this.reservePrice = reservePrice;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public Double getCurrentBid() {
		return currentBid;
	}

	public void setCurrentBid(Double currentBid) {
		this.currentBid = currentBid;
	}

	public String getBidderName() {
		return bidderName;
	}

	public void setBidderName(String bidderName) {
		this.bidderName = bidderName;
	}

	public String getAuctionItemId() {
		return auctionItemId;
	}
}
