package org.demo.auctionmanagement.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.demo.auctionmanagement.controller.AuctionController;
import org.demo.auctionmanagement.dto.AuctionItemDTO;
import org.demo.auctionmanagement.dto.AuctionItemDetailDTO;
import org.demo.auctionmanagement.dto.Bid;
import org.demo.auctionmanagement.dto.BidResponse;
import org.demo.auctionmanagement.entity.AuctionEntity;
import org.demo.auctionmanagement.repository.AuctionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AuctionService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AuctionService.class);
	
	@Autowired
	private AuctionRepository auctionRepository;

	public String createAuction(AuctionItemDTO auctionItemDTO) {
		AuctionEntity entity = new AuctionEntity();
		entity.setReservePrice(auctionItemDTO.getReservePrice());
		entity.setItemId(auctionItemDTO.getItem().getItemId());
		entity.setItemDescription(auctionItemDTO.getItem().getDescription());
		entity.setCurrentBid(0d);
		entity = auctionRepository.save(entity);
		return entity.getAuctionItemId();
	}
	
	private AuctionItemDTO convertEntityToDTO(AuctionEntity entity) {
		//TODO we can use json mapper for it
		AuctionItemDTO auction = new AuctionItemDTO();
		auction.setAuctionItemId(entity.getAuctionItemId());
		auction.setBidderName(entity.getBidderName());
		auction.setCurrentBid(entity.getCurrentBid());
		auction.setItem(new AuctionItemDetailDTO(entity.getItemId(), entity.getItemDescription()));
		auction.setReservePrice(entity.getReservePrice());
		return auction;
		
	}
	
	public AuctionItemDTO getAuctionById(String auctionId) {
		Optional<AuctionEntity> optional = auctionRepository.findById(auctionId);
		AuctionItemDTO auction = new AuctionItemDTO();
		if(optional.isPresent()) {
			auction = convertEntityToDTO(optional.get());
		}else {
			LOGGER.error("Auction Item with id {} not found",auctionId);
		}
		return auction;
	}

	public List<AuctionItemDTO> getAuctions() {
		List<AuctionEntity> entities = auctionRepository.findAll();
		List<AuctionItemDTO> auctionItemDTOs = new ArrayList<AuctionItemDTO>();
		if(entities != null && entities.size() > 0) {
			entities.forEach(entity -> {
				auctionItemDTOs.add(convertEntityToDTO(entity));
			});
		}
		return auctionItemDTOs;
	}

	public HttpEntity<BidResponse> placeBid(Bid bid) {
		HttpEntity<BidResponse> response;
		BidResponse bidResponse = new BidResponse();
		AuctionEntity entity = auctionRepository.getOne(bid.getAuctionItemId());
		if(entity != null) {
			if( bid.getMaxAutoBidAmount() < entity.getReservePrice()) {
				Double max = entity.getCurrentBid() > bid.getMaxAutoBidAmount() ? entity.getCurrentBid() : bid.getMaxAutoBidAmount();
				entity.setCurrentBid(max);
				LOGGER.warn("Bid price {} is less than reserve price {}.",bid.getMaxAutoBidAmount(),entity.getReservePrice());
				bidResponse.setResponse("Bid Price less than reserve price");
			}else {
				entity.setReservePrice(bid.getMaxAutoBidAmount() + 1);
				entity.setCurrentBid(bid.getMaxAutoBidAmount());
				entity.setBidderName(bid.getBidderName());
				LOGGER.warn("Other user only need $1 bid more than {} to replace bid", entity.getReservePrice());
				bidResponse.setResponse("Other bidder only need $1 more than your bid to replace your bid");
			}
			auctionRepository.save(entity);
			response = new ResponseEntity<BidResponse>(bidResponse,HttpStatus.OK);
		}else {
			bidResponse.setResponse("Invalid Auction id");
			response = new ResponseEntity<BidResponse>(bidResponse,HttpStatus.BAD_REQUEST);
		}
		return response;
	}

}
