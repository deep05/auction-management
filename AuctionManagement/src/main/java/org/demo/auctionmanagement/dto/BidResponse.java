package org.demo.auctionmanagement.dto;

import org.springframework.stereotype.Component;

@Component
public class BidResponse {
	private String response;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
