package org.demo.auctionmanagement.dto;

import org.springframework.stereotype.Component;

@Component
public class AuctionItemDetailDTO {
	private String itemId;
	private String description;
	
	public AuctionItemDetailDTO() {
	}
	public AuctionItemDetailDTO(String itemId, String description) {
		this.itemId = itemId;
		this.description = description;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
