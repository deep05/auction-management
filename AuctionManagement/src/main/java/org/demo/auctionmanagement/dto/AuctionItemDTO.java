package org.demo.auctionmanagement.dto;

import org.springframework.stereotype.Component;

@Component
public class AuctionItemDTO {
	private String auctionItemId;
	private Double reservePrice;
	private AuctionItemDetailDTO item;
	private Double currentBid;
	private String bidderName;

	public String getAuctionItemId() {
		return auctionItemId;
	}

	public void setAuctionItemId(String auctionItemId) {
		this.auctionItemId = auctionItemId;
	}

	public Double getReservePrice() {
		return reservePrice;
	}

	public void setReservePrice(Double reservePrice) {
		this.reservePrice = reservePrice;
	}

	public AuctionItemDetailDTO getItem() {
		return item;
	}

	public void setItem(AuctionItemDetailDTO item) {
		this.item = item;
	}

	public Double getCurrentBid() {
		return currentBid;
	}

	public void setCurrentBid(Double currentBid) {
		this.currentBid = currentBid;
	}

	public String getBidderName() {
		return bidderName;
	}

	public void setBidderName(String bidderName) {
		this.bidderName = bidderName;
	}
	
}
