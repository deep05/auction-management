package org.demo.auctionmanagement.controller;

import java.util.List;

import org.demo.auctionmanagement.dto.AuctionItemDTO;
import org.demo.auctionmanagement.dto.Bid;
import org.demo.auctionmanagement.dto.BidResponse;
import org.demo.auctionmanagement.service.AuctionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
//TODO cross implementation using property file
public class AuctionController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AuctionController.class);
	
	@Autowired
	private AuctionService auctionService;

	@PostMapping("/auctionItems")
	public HttpEntity<String> createAuctionItem(@RequestBody AuctionItemDTO auctionItemDTO) {
		// TODO Validator on Auction DTO
		LOGGER.info("Auction Item creation begins");
		String auctionItemId = auctionService.createAuction(auctionItemDTO);
		LOGGER.info("Auction Item created with id {}",auctionItemId );
		return new ResponseEntity<String>(auctionItemId, HttpStatus.OK);
	}

	@GetMapping("/auctionItems")
	public HttpEntity<List<AuctionItemDTO>> getAuctionItems() {
		List<AuctionItemDTO> auctionItemDTOs = auctionService.getAuctions();
		LOGGER.info("Fetching list of auction items.");
		return new ResponseEntity<List<AuctionItemDTO>>(auctionItemDTOs, HttpStatus.OK);
	}

	@GetMapping("/auctionItems/{auctionItemId}")
	public HttpEntity<AuctionItemDTO> getAuctionItem(@PathVariable String auctionItemId) {
		LOGGER.info("Fetching auction items with id {}",auctionItemId);
		AuctionItemDTO auctionItemDTO = auctionService.getAuctionById(auctionItemId);
		return new ResponseEntity<AuctionItemDTO>(auctionItemDTO, HttpStatus.OK);
	}

	@PostMapping("/bids")
	public HttpEntity<BidResponse> createAuctionItem(@RequestBody Bid bid) {
		// TODO Validator on BID DTO
		LOGGER.info("Placing bid for auction items with id {}",bid.getAuctionItemId());
		return auctionService.placeBid(bid);
	}

}
