import { Component,OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  showCreateScreen : boolean = false;
  showBidScreen:boolean =false;
  reservePrice: number;
  itemId: string;
  description: string;
  auctionItems: any;
  auctionItemId:string;
  bidderName:string;
  bidPrice:number;
  bidResponse:any;

  readonly baseUrl: string = "http://localhost:8080"

  constructor(private http: HttpClient){}
  
  ngOnInit(){
  	this.getAuctionItems();
  }

  createAuctionItem() {
    this.showCreateScreen = false;
    let auctionItem = {
      reservePrice : this.reservePrice,
      item : {
        itemId : this.itemId,
        description: this.description
      }
    };
    this.resetAuctionFields();
    this.http.post(this.baseUrl+"/auctionItems",auctionItem).subscribe( response => {
      this.getAuctionItems();
    });
  }

  getAuctionItems() {
    this.http.get(this.baseUrl+"/auctionItems").subscribe( response => {
      this.auctionItems = response;
    });
  }

  placeBid(auctionId : string) {
    this.showBidScreen = false;

    let bidItem = {
      auctionItemId: this.auctionItemId,
      maxAutoBidAmount: this.bidPrice,
      bidderName: this.bidderName
    };
    this.resetBidFields();
    this.http.post(this.baseUrl+"/bids",bidItem).subscribe(response => {
      this.bidResponse = response;
      alert(this.bidResponse.response);
      this.getAuctionItems();
    });
  }
  
  resetBidFields() {
  	this.auctionItemId = null;
  	this.bidPrice = null;
  	this.bidderName = null;
  }
  
  resetAuctionFields() {
  	this.reservePrice = null;
  	this.itemId = null;
  	this.description = null;
  }
  
}
